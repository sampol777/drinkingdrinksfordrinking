package com.example.drinkingdrinksfordrinking.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.drinkingdrinksfordrinking.model.DrinksRepo
import com.example.drinkingdrinksfordrinking.model.remote.response.CategoryResponse
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkDetails
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkResponse
import kotlinx.coroutines.launch

class DrinksViewModel(val repo: DrinksRepo) : ViewModel() {
    private val _catState: MutableLiveData<CategoryState> = MutableLiveData(CategoryState())
    val catState: LiveData<CategoryState> get() = _catState

    private val _drinkState: MutableLiveData<DrinkState> = MutableLiveData(DrinkState())
    val drinkState: LiveData<DrinkState> get() = _drinkState

    private val _detailState: MutableLiveData<DetailsState> = MutableLiveData(DetailsState())
    val detailState: LiveData<DetailsState> get() = _detailState

    fun getCategories() {
        viewModelScope.launch {
            _catState.value = _catState.value?.copy(isLoading = true)
            _catState.value = _catState.value?.copy(isLoading = false, items = repo.getCategories())
        }
    }

    fun getDrinks(category: String) {
        viewModelScope.launch {
            _drinkState.value = _drinkState.value?.copy(item = category, isLoading = true)
            _drinkState.value = _drinkState.value?.copy(isLoading = false, items = repo.getDrinks(category))


        }
    }

    fun getDetails(drinkId: Int, drinkName: String) {
        viewModelScope.launch {
            _detailState.value = _detailState.value?.copy( isLoading = true)
            _detailState.value = _detailState.value?.copy(isLoading = false, items = repo.getDetails(drinkId))
        }
    }

    data class DrinkState(
        val isLoading: Boolean = false,
        val item: String = "",
        val items: DrinkResponse = DrinkResponse(emptyList())
    )

    data class CategoryState(
        val isLoading: Boolean = false,
        val item: String = "Categories",
        val items: CategoryResponse = CategoryResponse(emptyList())
    )

    data class DetailsState(
        val isLoading: Boolean = false,
        val items: DrinkDetails = DrinkDetails(emptyList())
    )
}