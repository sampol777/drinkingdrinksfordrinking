package com.example.drinkingdrinksfordrinking.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.drinkingdrinksfordrinking.model.DrinksRepo

class DrinksVMFactory(
    private val repo:DrinksRepo
):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create (ModelClass:Class<T>): T {
        return DrinksViewModel(repo) as T
    }
}