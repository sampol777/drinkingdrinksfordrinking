package com.example.drinkingdrinksfordrinking.model

import com.example.drinkingdrinksfordrinking.model.remote.DrinksService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object DrinksRepo {
    private val drinksService = DrinksService.getInstance()

    suspend fun getCategories() = withContext(Dispatchers.IO){
        return@withContext drinksService.getCategories()
    }

    suspend fun  getDrinks(category:String)= withContext(Dispatchers.IO){
        return@withContext drinksService.getDrinks(category)
    }

    suspend fun getDetails(id:Int) = withContext(Dispatchers.IO){
        return@withContext drinksService.getDetails(id)
    }
}