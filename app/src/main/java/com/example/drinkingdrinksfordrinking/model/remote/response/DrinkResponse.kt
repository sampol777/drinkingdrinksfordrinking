package com.example.drinkingdrinksfordrinking.model.remote.response

data class DrinkResponse(
    val drinks: List<Drink>
){
    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}