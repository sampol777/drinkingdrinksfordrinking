package com.example.drinkingdrinksfordrinking.model.remote

import com.example.drinkingdrinksfordrinking.model.remote.response.CategoryResponse
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkDetails
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinksService {
    companion object {
        private const val BASE_URL: String = "https://www.thecocktaildb.com"
        private const val ENDPOINT: String = "/api/json/v1/1/"
        private const val CAT_ROUTE = "${ENDPOINT}list.php" //?c=list
        private const val DRINK_ROUTE = "${ENDPOINT}filter.php" //?c=Ordinary_Drink
        private const val DETAIL_ROUTE = "${ENDPOINT}lookup.php" //?i=11007

        fun getInstance(): DrinksService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()

    }

    @GET(CAT_ROUTE)
    suspend fun getCategories(@Query("c") category: String = "list"): CategoryResponse

    @GET(DRINK_ROUTE)
    suspend fun getDrinks(@Query("c") category: String): DrinkResponse

    @GET(DETAIL_ROUTE)
    suspend fun  getDetails(@Query("i") drinkId: Int): DrinkDetails

}