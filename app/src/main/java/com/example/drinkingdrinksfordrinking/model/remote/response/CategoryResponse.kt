package com.example.drinkingdrinksfordrinking.model.remote.response

import com.google.gson.annotations.SerializedName

data class CategoryResponse(
    @SerializedName("drinks")
    val categories: List<Category>
){
    data class Category(
        val strCategory: String
    )
}