package com.example.drinkingdrinksfordrinking.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.drinkingdrinksfordrinking.databinding.FragmentIngredientBinding
import com.example.drinkingdrinksfordrinking.model.DrinksRepo
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkDetails
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksVMFactory
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksViewModel
import java.security.Key
import java.util.*

class IngredientsFragment : Fragment() {
    lateinit var binding: FragmentIngredientBinding
    private val vmFactory: DrinksVMFactory = DrinksVMFactory(DrinksRepo)
    private val viewModel by viewModels<DrinksViewModel>() { vmFactory }
    val args: IngredientsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentIngredientBinding.inflate(
            inflater,
            container,
            false
        )
        viewModel.getDetails(args.drinkId, args.drinkName)
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {
        Log.e("TAG", "initViews: $args")
        binding.name.text = args.drinkName
    }

    fun initObservers() {
        viewModel.detailState.observe(viewLifecycleOwner) { state ->
            binding.foreverSpinner.isVisible = state.isLoading
            displayingDrink(state)


        }
    }

    fun displayingDrink(state: DrinksViewModel.DetailsState) {
        with(binding) {
            state.items.drinks.firstOrNull()?.let {
                image.load(it.strDrinkThumb)
                val s = "${it.strInstructions} ${it.toIngredientsList()}"
                details.text = s

            }


        }
    }
}