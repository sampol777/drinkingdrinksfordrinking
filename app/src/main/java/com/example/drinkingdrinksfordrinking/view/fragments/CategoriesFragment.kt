package com.example.drinkingdrinksfordrinking.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.drinkingdrinksfordrinking.databinding.FragmentCategoriesBinding
import com.example.drinkingdrinksfordrinking.model.DrinksRepo
import com.example.drinkingdrinksfordrinking.view.CategoryAdapter
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksVMFactory
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksViewModel

class CategoriesFragment : Fragment() {
    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!
    private val vmFactory: DrinksVMFactory = DrinksVMFactory(DrinksRepo)
    private val viewModel by viewModels<DrinksViewModel>() { vmFactory }

    val categoryAdapter = CategoryAdapter { category ->
        val action =
            CategoriesFragmentDirections.actionCategoriesFragmentToDrinksFragment(category)
        findNavController().navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoriesBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()

    }

    fun initViews() {
        with(binding.categoriesList) {
            layoutManager = GridLayoutManager(this@CategoriesFragment.context, 1)
            adapter = categoryAdapter
        }
    }

    fun initObservers() {
        viewModel.catState.observe(viewLifecycleOwner) { state ->
            binding.foreverSpinner.isVisible = state.isLoading
            categoryAdapter.updateList(state.items.categories)

        }

        viewModel.getCategories()
    }


}