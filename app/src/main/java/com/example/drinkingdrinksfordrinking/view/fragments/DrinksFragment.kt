package com.example.drinkingdrinksfordrinking.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.drinkingdrinksfordrinking.databinding.FragmentDrinksBinding
import com.example.drinkingdrinksfordrinking.model.DrinksRepo
import com.example.drinkingdrinksfordrinking.view.CategoryAdapter
import com.example.drinkingdrinksfordrinking.view.DrinksAdapter
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksVMFactory
import com.example.drinkingdrinksfordrinking.viewmodel.DrinksViewModel

class DrinksFragment: Fragment() {
    lateinit var binding: FragmentDrinksBinding
    private val vmFactory: DrinksVMFactory = DrinksVMFactory(DrinksRepo)
    private val viewModel by viewModels<DrinksViewModel>() { vmFactory }
    val args : DrinksFragmentArgs by navArgs()

    val drinksAdapter = DrinksAdapter { drinkName, drinkId ->
        val action =
            DrinksFragmentDirections.actionDrinksFragmentToDetailsPageFragment(drinkName, drinkId)
        findNavController().navigate(action)

    }

    override fun onCreateView(
        inflater:LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinksBinding.inflate(inflater,container,false)
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {

        with(binding.drinksList){
            layoutManager = GridLayoutManager(this@DrinksFragment.context, 1)
            adapter = drinksAdapter
        }
    }

    fun initObservers() {
        viewModel.drinkState.observe(viewLifecycleOwner) { state ->
            binding.foreverSpinner.isVisible = state.isLoading
            drinksAdapter.updateList(state.items.drinks)
        }

        viewModel.getDrinks(category = args.category)
    }
}