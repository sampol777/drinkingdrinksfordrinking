package com.example.drinkingdrinksfordrinking.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.drinkingdrinksfordrinking.databinding.DrinkItemBinding
import com.example.drinkingdrinksfordrinking.model.remote.response.DrinkResponse

class DrinksAdapter(private val adapterListener: (drinkName: String, drinkId: Int) -> Unit) :
    RecyclerView.Adapter<DrinksAdapter.DrinksViewHolder>() {

    private var list: MutableList<DrinkResponse.Drink> = mutableListOf()

    fun updateList(newList: List<DrinkResponse.Drink>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class DrinksViewHolder(
        private val binding: DrinkItemBinding,
        private val vhListener: (drinkName: String, drinkId: Int) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun displayDrinks(drink: DrinkResponse.Drink) {
            binding.root.setOnClickListener {
                vhListener.invoke(drink.strDrink, drink.idDrink.toInt())
            }
            binding.name.text = drink.strDrink
            binding.image.load(drink.strDrinkThumb)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(
            DrinkItemBinding.inflate(LayoutInflater.from(parent.context)),
            adapterListener
        )
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        holder.displayDrinks(list[position])
    }

    override fun getItemCount(): Int = list.size
}


